﻿using ASPNET_CORE_USING_SQL_LOCALIZATION.Models;
using Localization.SqlLocalizer.IntegrationTests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace ASPNET_CORE_USING_SQL_LOCALIZATION.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStringLocalizer<SharedResource> _localizer;
        private readonly IStringLocalizer<HomeController> _homeLocalizer;

        public HomeController(IStringLocalizer<SharedResource> localizer, IStringLocalizer<HomeController> homeLocalizer)
        {
            // Accessible by all Controllers
            _localizer = localizer;
            // Only for HomeController
            _homeLocalizer = homeLocalizer;
        }

        public ActionResult Index(HomeModel model)
        {
            model.Title = "Localization String: " + _homeLocalizer["Greeting"];
            model.AnotherTitle = "Another Localization String: " + _homeLocalizer["ShortParagraph"];
            model.HomeParagraph = "A short paragraph: " + _homeLocalizer["HomeParagraph"];

            return View(model);
        }
    }
}
