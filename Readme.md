﻿# Description
This is an example project based on https://damienbod.com/2016/01/29/asp-net-core-1-0-using-sql-localization/.
The objective is to build an app that demonstrate the localization like display web content 
with different language selection. 

# Try it out
```
// Clone the project to your local drive
git clone https://gitlab.com/AizenSousuke/localization-example.git
// Go to the project folder
cd "localization-example\ASPNET CORE USING SQL LOCALIZATION"
// Restore the dependencies
dotnet restore
// Run the project
dotnet run
```

Finally go to the url that's shown

https://localhost:5001/

https://localhost:5001/Home
