﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPNET_CORE_USING_SQL_LOCALIZATION.Models
{
    public class HomeModel
    {
        public string Title { get; set; }
        public string AnotherTitle { get; set; }
        public string HomeParagraph { get; set; }
    }
}
